import sys
sys.path.insert(1,'API/')
import imp

modules=['argparse','random','numpy','cPickle','sklearn']
for module in modules:
	try:
           imp.find_module(module)
        except ImportError:
           print 'Error'
	   print 'you don`t have following dependancy module :', module
	   if module=='sklearn':
		print 'for Ubuntu :'
		print '\t\t ::  sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose'
		print '\t\t :: install scikit-learn-0.15.2 given in HindiAnaphorav0.4/library folder'

		print 'for fedora :'
		print '\t\t  :: sudo yum install numpy scipy python-matplotlib ipython python-pandas sympy python-nose'
		print '\t\t :: install scikit-learn-0.15.2 given in HindiAnaphorav0.4/library folder'
	   exit()






import codecs
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
import cPickle
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier 
import random
import numpy as np
from sklearn.ensemble import RandomForestClassifier 
import argparse







parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', '-please specify the input file path')
parser.add_argument('-o', '--output', '-please specify the output model path')
args = parser.parse_args()


if args.input==None or args.output==None:
	print 'For help please use: python posttraining.py -h'
	exit()


file=codecs.open(args.input)
data=file.readlines()
flag=False
testDATASET={}
class_tr=[]
count=0
for i in data:
	if i.strip()=='@data':
		flag=True
		continue
	if flag:
		d_i=i.strip().split(',')
		lenth=0
		testDATASET[count]={}
		for j in d_i:
			lenth=lenth+1
			if lenth!=len(d_i):
				if lenth==1:
					testDATASET[count]['anaphora']=j.strip()
				if lenth==2:
					testDATASET[count]['anaPRP_type']=j.strip()
				if lenth==3:
                                	testDATASET[count]['anasemprop']=j.strip()
				if lenth==4:
	                                testDATASET[count]['anaperson']=j.strip()
				if lenth==5:
	                                testDATASET[count]['ananumber']=j.strip()
				if lenth==6:
	                                testDATASET[count]['antPOSTag']=j.strip()
				if lenth==7:
	                                testDATASET[count]['antNN']=int(j.strip())
				if lenth==8:
	                                testDATASET[count]['antNNP']=int(j.strip())
				if lenth==9:
	                                testDATASET[count]['antPRP']=int(j.strip())
				if lenth==10:
	                                testDATASET[count]['antOTR']=int(j.strip())
				if lenth==11:
	                                testDATASET[count]['antsemprop']=j.strip()
				if lenth==12:
	                                testDATASET[count]['antperson']=j.strip()
				if lenth==13:
	                                testDATASET[count]['antnumber']=j.strip()
				if lenth==14:
	                                testDATASET[count]['distance']=int(j.strip())
				if lenth==15:
	                                testDATASET[count]['stm_distance']=int(j.strip())
				if lenth==16:
	                                testDATASET[count]['DEM']=int(j.strip())
			else:
				class_tr.append(j.strip())
		count=count+1
dis=[]
final_X=[]
for i in testDATASET:
    dis.append(testDATASET[i])
vec=DictVectorizer(sparse=False)
X=vec.fit_transform(dis)

print '\n\n\n\n\n\n Training started \n\n\n\n\n\n\n'

clf=tree.DecisionTreeClassifier()
clf=clf.fit(X,class_tr)
with open(args.output, 'wb') as fid:
    cPickle.dump(clf, fid)
