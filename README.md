Module Name: anaphora-scikit-hin-0.4 (anaphora resolution for Hindi):
---------------------------------------------------------------------------

 Operating System		:	LINUX (tested on Fedora-19)


For this module to run following prerequisites module`s output needed: 
----------------------------------------------------------------------
	=>output of fullparser
	=>Input tho this module requires UTF-8 script
	  	note:please look at testinputsimple folder in root(anaphora-scikit-hin-0.4/tests/testcasenestedUTF) 
		folder for test input files



Library needed: 
---------------
			Python, Perl and shell script
				and 
    			Scikit-learn(0.15.2) 
			which requires:
    				 Python (>= 2.6 or >= 3.3),
    				 NumPy (>= 1.6.1),
    				 SciPy (>= 0.9).


To install Scikit-learn(scikit-0.15.2) (on Ubuntu):
---------------------------------------------------------
		1)sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose
		2) For scikit-0.15.2 installation
			2.1) go to anaphora-scikit-hin-0.4/library/
			2.2) tar -xvf scikit-0.15.2.tar.gz
			2.3) cd scikit-0.15.2/
			2.4) python setup.py build
			2.3) sudo python setup.py install



To install Scikit-learn (scikit-0.15.2)(on Fedora):
---------------------------------------------------------
		1)sudo yum install numpy scipy python-matplotlib ipython python-pandas sympy python-nose
		2) For scikit-0.15.2 installation
			2.1) go to anaphora-scikit-hin-0.4/library/
			2.2) tar -xvf scikit-0.15.2.tar.gz
			2.3) cd scikit-0.15.2/
			2.4) python setup.py build
			2.3) sudo python setup.py install




how to run
---------------

Ex (using script)
=================
   
   sh anaphora-scikit-hin_run.sh tests/testcasenestedUTF/parse1.in

   where second argunment is test input file


	Inside module wise run 
	-----------------------

	1) marking semprop in ssf file (input strickly in utf-8 form)

		python marksemprop.py -i tests/testcasenestedUTF/parse1.in -o /TMP.tmp/tempsemprop.out

		usage: marksemprop.py [-h] [-i INPUT] [-o OUTPUT]

		optional arguments:
	  	-h, --help            show this help message and exit
	  	-i INPUT, --input INPUT, -please specify the input file path INPUT
	  	-o OUTPUT, --output OUTPUT, -please specify the output file path [optional] OUTPUT


		note- input & output file must be needed

	2) marking anaphora relation in ssf file 
		usage: anaphora_resolution_main.py [-h] [-i INPUT] [-p PATH]
	
		python anaphora_resolution_main.py -p ../anaphora-scikit-hin-0.4/ -i /TMP.tmp/tempsemprop.out -o /TMP.tmp/tempanaphora.out

		optional arguments:
		-h, --help            show this help message and exit
	  	-i INPUT, --input INPUT, -please specify the input file path INPUT
	  	-p PATH, --path PATH, -please specify the anaphoraresolver tool`s path PATH
		-o OUTPUT, --output OUTPUT, -please specify the output file path [optional] OUTPUT

		note- input file must be needed output will print on console

	3) To copy referent feature to anaphora


		python featurecopyanaphora.py -i /TMP.tmp/tempanaphora.out

		usage: featurecopyanaphora.py [-h] [-i INPUT]
		optional arguments:
	  	-h, --help            show this help message and exit
	  	-i INPUT, --input INPUT, -please specify the input file path INPUT
		-o OUTPUT, --output OUTPUT, -please specify the output file path [optional] OUTPUT



Directory Structure:
-----------------------
anaphora-scikit-hin-0.4/
├── API
├── build
│   ├── lib.linux-i686-2.7
│   │   └── sklearn
│   │       ├── __check_build
│   │       ├── cluster
│   │       │   ├── bicluster
│   │       │   │   └── tests
│   │       │   └── tests
│   │       ├── covariance
│   │       │   └── tests
│   │       ├── cross_decomposition
│   │       ├── datasets
│   │       │   └── tests
│   │       ├── decomposition
│   │       │   └── tests
│   │       ├── ensemble
│   │       │   └── tests
│   │       ├── externals
│   │       │   └── joblib
│   │       │       └── test
│   │       ├── feature_extraction
│   │       │   └── tests
│   │       ├── feature_selection
│   │       │   └── tests
│   │       ├── gaussian_process
│   │       │   └── tests
│   │       ├── linear_model
│   │       │   └── tests
│   │       ├── manifold
│   │       ├── metrics
│   │       │   ├── cluster
│   │       │   │   ├── bicluster
│   │       │   │   │   └── tests
│   │       │   │   └── tests
│   │       │   └── tests
│   │       ├── mixture
│   │       │   └── tests
│   │       ├── neighbors
│   │       ├── neural_network
│   │       ├── preprocessing
│   │       ├── semi_supervised
│   │       ├── svm
│   │       │   └── tests
│   │       ├── tests
│   │       ├── tree
│   │       │   └── tests
│   │       └── utils
│   │           ├── sparsetools
│   │           └── tests
│   └── temp.linux-i686-2.7
│       └── sklearn
│           ├── __check_build
│           ├── cluster
│           ├── datasets
│           ├── ensemble
│           ├── feature_extraction
│           ├── linear_model
│           ├── manifold
│           ├── metrics
│           │   └── cluster
│           ├── neighbors
│           ├── src
│           │   └── cblas
│           ├── svm
│           │   └── src
│           │       ├── liblinear
│           │       └── libsvm
│           ├── tree
│           └── utils
│               ├── sparsetools
│               └── src
├── data
├── doc
│   ├── datasets
│   │   └── __pycache__
│   ├── developers
│   ├── images
│   ├── includes
│   ├── logos
│   ├── modules
│   │   └── glm_data
│   ├── __pycache__
│   ├── sphinxext
│   │   ├── numpy_ext
│   │   │   └── __pycache__
│   │   └── __pycache__
│   ├── templates
│   ├── testimonials
│   │   └── images
│   ├── themes
│   │   └── scikit-learn
│   │       └── static
│   │           ├── css
│   │           ├── img
│   │           └── js
│   └── tutorial
│       ├── basic
│       ├── common_includes
│       ├── machine_learning_map
│       ├── statistical_inference
│       └── text_analytics
│           ├── data
│           │   ├── languages
│           │   ├── movie_reviews
│           │   └── twenty_newsgroups
│           ├── __pycache__
│           ├── skeletons
│           └── solutions
├── doc_anaphora
├── examples
│   ├── applications
│   ├── bicluster
│   ├── cluster
│   ├── covariance
│   ├── cross_decomposition
│   ├── datasets
│   ├── decomposition
│   ├── ensemble
│   ├── exercises
│   ├── gaussian_process
│   ├── linear_model
│   ├── manifold
│   ├── mixture
│   ├── neighbors
│   ├── semi_supervised
│   ├── svm
│   └── tree
├── model
├── scikit_learn.egg-info
├── sklearn
│   ├── __check_build
│   ├── cluster
│   │   ├── bicluster
│   │   │   └── tests
│   │   └── tests
│   ├── covariance
│   │   └── tests
│   ├── cross_decomposition
│   ├── datasets
│   │   ├── data
│   │   ├── descr
│   │   ├── images
│   │   └── tests
│   │       └── data
│   ├── decomposition
│   │   └── tests
│   ├── ensemble
│   │   └── tests
│   ├── externals
│   │   └── joblib
│   │       └── test
│   ├── feature_extraction
│   │   └── tests
│   ├── feature_selection
│   │   └── tests
│   ├── gaussian_process
│   │   └── tests
│   ├── linear_model
│   │   └── tests
│   ├── manifold
│   ├── metrics
│   │   ├── cluster
│   │   │   ├── bicluster
│   │   │   │   └── tests
│   │   │   └── tests
│   │   └── tests
│   ├── mixture
│   │   └── tests
│   ├── neighbors
│   ├── neural_network
│   ├── preprocessing
│   ├── semi_supervised
│   ├── src
│   │   └── cblas
│   ├── svm
│   │   ├── src
│   │   │   ├── liblinear
│   │   │   └── libsvm
│   │   └── tests
│   ├── tests
│   ├── tree
│   │   └── tests
│   └── utils
│       ├── sparsetools
│       ├── src
│       └── tests
├── src
├── tests
│   ├── testcasenestedUTF
│   └── testcasesimpleUTF
└── TMP.tmp









----------------------------------------    
Any Queries Contact:
----------------------------------------
   Vandan Mujadia
   Language Technologies Research Center
   IIIT Hyderabad
   REPORTING BUGS:
   ---------------
   Report anaphora-scikit-hin-0.4 bugs to (vandan.mujadia@research.iiit.ac.in)
-----------------------------------------
